include $(PWD)/src/toolchain/Makefile

.DEFAULT_GOAL :=
.PHONY: default
default: \
	toolchain \
	$(OUT_DIR)/pcsc_demo

.PHONY:
clean:
	rm -rf $(CACHE_DIR) $(OUT_DIR)

$(FETCH_DIR)/pcsc:
	$(call git_clone,$@,$(PCSC_REPO),$(PCSC_REF))

$(FETCH_DIR)/rust:
	$(call git_clone,$@,$(RUST_REPO),$(RUST_REF))

$(FETCH_DIR)/rust/build/x86_64-unknown-linux-gnu/stage0-sysroot: \
	$(FETCH_DIR)/rust
	$(call toolchain," \
		cd $(FETCH_DIR)/rust \
		&& git submodule update --init \
		&& ./configure \
			--set="build.rustc=/usr/bin/rustc" \
			--set="build.cargo=/usr/bin/cargo" \
			--set="target.x86_64-unknown-linux-musl.llvm-config=/usr/bin/llvm-config" \
			--set="target.x86_64-unknown-linux-musl.musl-libdir=/usr/lib/x86_64-linux-musl" \
		&& python3 x.py build \
			--stage 0 \
			--target x86_64-unknown-linux-musl \
			library \
	")

$(CACHE_DIR)/lib/libpcsclite.a: \
	$(FETCH_DIR)/pcsc
	$(call toolchain," \
		cd $(FETCH_DIR)/pcsc \
		&& export CC=musl-gcc \
		&& export CXX=musl-g++ \
		&& export CFLAGS=-static \
		&& export CXXFLAGS=-static \
		&& ./bootstrap \
		&& ./configure \
			--enable-static \
			--disable-polkit \
			--disable-strict \
			--disable-libsystemd \
			--disable-libudev \
			--disable-libusb \
		&& make \
		&& mkdir -p /home/build/$(CACHE_DIR)/lib \
		&& cp src/.libs/libpcsclite.a /home/build/$@ \
	")

$(OUT_DIR)/pcsc_demo: \
	$(CACHE_DIR)/lib/libpcsclite.a \
	$(FETCH_DIR)/rust/build/x86_64-unknown-linux-gnu/stage0-sysroot
	$(call toolchain," \
		cd $(SRC_DIR) \
		&& export CARGO_HOME=$(CACHE_DIR) \
		&& export RUSTFLAGS=' \
			-L /home/build/$(FETCH_DIR)/rust/build/x86_64-unknown-linux-gnu/stage0-sysroot/lib/rustlib/x86_64-unknown-linux-musl/lib/ \
			-L /home/build/$(FETCH_DIR)/rust/build/x86_64-unknown-linux-gnu/stage0-sysroot/lib/rustlib/x86_64-unknown-linux-musl/lib/self-contained/ \
			-L /usr/lib/x86_64-linux-musl \
			-C target-feature=+crt-static \
		' \
		&& export PCSC_LIB_DIR='/home/build/$(CACHE_DIR)/lib' \
		&& export PCSC_LIB_NAME='static=pcsclite' \
		&& cargo build \
			-v \
			--target x86_64-unknown-linux-musl \
			--no-default-features \
			--release \
		&& cp \
			target/x86_64-unknown-linux-musl/release/pcsc_demo \
			/home/build/$@; \
	")
